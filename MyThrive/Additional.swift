//
//  Additional.swift
//  qwix
//
//  Created by Алексей on 06.02.15.
//  Copyright (c) 2015 StudioMobile. All rights reserved.
//

import UIKit

func shortUUID() -> String {
    return NSUUID().uuidString.components(separatedBy: "-").last!
}

func debounce(delay:Int, queue:DispatchQueue, action: @escaping (()->())) -> ()->() {
    var lastFireTime = DispatchTime.now()
    let dispatchDelay = DispatchTimeInterval.milliseconds(delay)
    
    return {
        let dispatchTime: DispatchTime = lastFireTime + dispatchDelay
        queue.asyncAfter(deadline: dispatchTime, execute: {
            let when: DispatchTime = lastFireTime + dispatchDelay
            let now = DispatchTime.now()
            if now.rawValue >= when.rawValue {
                lastFireTime = DispatchTime.now()
                action()
            }
        })
    }
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

func background(_ closure:@escaping ()->()) {
    DispatchQueue.global(qos: .background).async {
        closure()
    }
}

func main(_ closure:@escaping ()->()) {
    DispatchQueue.main.async(execute: { () -> Void in
        closure()
    })
}

func getImageWithInset(_ image : UIImage, size : CGSize, inset : UIEdgeInsets) -> UIImage {
    let newSize = CGSize(width: size.width + inset.left + inset.right, height: size.height + inset.top + inset.bottom)
    
    UIGraphicsBeginImageContextWithOptions(newSize, true, UIScreen.main.scale)
    UIColor.white.set()
    UIRectFill(CGRect(x: -1.0, y: -1.0, width: newSize.width + 2, height: newSize.height + 2));
    image.draw(in: CGRect(x: inset.left, y: inset.top, width: size.width, height: size.height))
    let resImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    
    return resImage
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    var intValue: Int {
        return (self as NSString).integerValue
    }
    var length : Int {
        return self.characters.count
    }

    mutating func insertCharacter(atIndex index: Int, _ newChar: Character) -> Void {
        var modifiedString = String()
        for (i, char) in self.characters.enumerated() {
            modifiedString += String((i == index) ? newChar : char)
        }
        self = modifiedString
    }

    
    mutating func replaceCharacter(atIndex index: Int, _ newChar: Character) -> Void {
        var modifiedString = String()
        for (i, char) in self.characters.enumerated() {
            modifiedString += String((i == index) ? newChar : char)
        }
        self = modifiedString
    }
    mutating func removeCharacter(atIndex index: Int) -> Void {
        var modifiedString = String()
        for (i, char) in self.characters.enumerated() {
            if (i != index) {
                modifiedString += String(char)
            }
        }
        self = modifiedString
    }
}

extension UIImageView {
    func tintWithColor(_ color : UIColor?) {
        var image : UIImage!
        if color == nil {
            image = self.image?.withRenderingMode(.alwaysOriginal)
            self.tintColor = nil
        } else {
            image = self.image?.withRenderingMode(.alwaysTemplate)
            self.tintColor = color
        }
        self.image = image
    }
}

extension UIButton {
    
    func setTitle(_ title : String?){
        self.setTitle(title, for: UIControlState())
    }
    
    func setImage(_ image : UIImage?){
        self.setImage(image, for: UIControlState())
    }
    
    func setBackgroundImage(_ image : UIImage){
        self.setBackgroundImage(image, for: UIControlState())
    }
    
    func addTarget(_ target: AnyObject?, action: Selector) {
        self.addTarget(target, action: action, for: UIControlEvents.touchUpInside)
    }
    
    func setTitleColor(_ color :UIColor){
        self.setTitleColor(color, for: UIControlState())
    }
    
    func tintWithColor(_ color : UIColor?) {
        var image : UIImage!
        if color == nil {
            image = self.imageView?.image?.withRenderingMode(.alwaysOriginal)
            self.tintColor = nil
        } else {
            image = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
            self.tintColor = color
        }
        self.setImage(image)
    }
}

extension Array {
    func contains<T>(_ obj: T) -> Bool where T : Equatable {

        return self.filter({$0 as? T == obj}).count > 0
    }
    
    mutating func removeObject<U: Equatable>(_ object: U) {
        var index: Int?
        for (idx, objectToCompare) in self.enumerated() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                    break
                }
            }
        }
        
        if(index != nil) {
            self.remove(at: index!)
        }
    }
    
    mutating func remove(_ object:AnyObject) -> Void
    {
        for (idx, element) in self.enumerated() {
                if object === element as AnyObject {
                    self.remove(at: idx)
                }
        }
    }
    
    func find (_ includedElement: (Element) -> Bool) -> Int? {
        for (idx, element) in self.enumerated() {
            if includedElement(element) {
                return idx
            }
        }
        return nil
    }
}

extension UIImage {
    
    func scaleImage(_ newSize: CGSize) -> UIImage {
        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .low
        let flipVertical = __CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height)
        context?.concatenate(flipVertical)
        context!.__draw(in: newRect, image: self.cgImage!)
        let newImage = UIImage(cgImage: (context?.makeImage()!)!)
        UIGraphicsEndImageContext()
        return newImage
    }
    
    class func addWatermarkToImage(_ image : UIImage) -> UIImage {
        if let watermark = UIImage(named : "watermark") {
            UIGraphicsBeginImageContextWithOptions(image.size, true, 0.0)
            image.draw(in: CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height))
            watermark.draw(in: CGRect(x: image.size.width - watermark.size.width , y: image.size.height - watermark.size.height, width: watermark.size.width, height: watermark.size.height))
            let result = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return result!
        }
        else {
            return image
        }
    }

    
    public func imageRotatedByDegrees(_ degrees: CGFloat, flip: Bool) -> UIImage {
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(Double.pi)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap?.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0);
        
        //   // Rotate the image context
        bitmap?.rotate(by: degreesToRadians(degrees));
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap?.scaleBy(x: yFlip, y: -1.0)
        bitmap!.__draw(in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height), image: cgImage!)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImageOrientation.up {
            return self.copy() as! UIImage
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform = CGAffineTransform.identity;
        
        switch self.imageOrientation {
        case .down, .downMirrored :
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2))
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: CGFloat(-Double.pi / 2))
        default:
            break
        }
        
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            
        default:
            break
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        
        let ctx = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height), bitsPerComponent: (self.cgImage?.bitsPerComponent)!, bytesPerRow: 0, space: (self.cgImage?.colorSpace!)!, bitmapInfo: (self.cgImage?.bitmapInfo.rawValue)!)
        ctx?.concatenate(transform)
        
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx!.__draw(in: CGRect(x: 0, y: 0, width: self.size.height,height: self.size.width), image: self.cgImage!)
            
        default:
            ctx!.__draw(in: CGRect(x: 0, y: 0, width: self.size.width,height: self.size.height), image: self.cgImage!)
        }
        
        // And now we just creat e a new UIImage from the drawing context
        let cgimg = ctx?.makeImage();
        return UIImage(cgImage: cgimg!)
    }
}

