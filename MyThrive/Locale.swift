//
//  Locale.swift
//  MyThrive
//
//  Created by Oleg Ulitsionak on 29.07.17.
//  Copyright © 2017 com.ololo. All rights reserved.
//

import UIKit

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

extension Array {
    var localized: [String] {
        if let array = self as? [String] {
            return array.map({$0.localized})
        }
        return self.map({_ in ""})
    }
}
