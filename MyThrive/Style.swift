//
//  Style.swift
//  qwix
//
//  Created by Алексей on 07.02.15.
//  Copyright (c) 2015 StudioMobile. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(r: Int, g: Int, b: Int) {
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: 1)
    }

    convenience init(r: Int, g: Int, b: Int, a: Int) {
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    convenience init(r: Int, g: Int, b: Int, aF: CGFloat) {
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: aF)
    }


    class func tealColor() -> UIColor {
        return UIColor(red: 102 / 255.0, green: 152 / 255.0, blue: 178 / 255.0, alpha: 1)
    }

    convenience init(white256: Int) {
        self.init(white: CGFloat(white256) / 255, alpha: 1)
    }

    convenience init(hex: String, alpha: CGFloat = 1.0) {
        var string = hex
        if (string.hasPrefix("#")) {
            let index = string.index(after: string.startIndex)
            string = string.substring(from: index)
        }

        if ((string.characters.count) != 6) {
            self.init()
        } else {

            var rgbValue: UInt32 = 0
            Scanner(string: string).scanHexInt32(&rgbValue)

            self.init(
                    red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0)
            )
        }
    }

    func hexValue() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb: Int = (Int)(r * 255) << 16 | (Int)(g * 255) << 8 | (Int)(b * 255) << 0

        return String(format: "#%06x", rgb)
    }
}

extension UIFont {

    class func pangolin(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Regular", size: size)!
    }


    class func light(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    }

    class func normal(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size)!
    }

    class func regular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size)!
    }

    class func bold(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }

    class func italic(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Italic", size: size)!
    }

    class func centuryGothicRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue", size: size)!
    }
    
    class func centuryGothicBold(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Bold", size: size)!
    }

}




