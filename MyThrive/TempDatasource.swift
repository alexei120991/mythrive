//
//  TempDatasource.swift
//  MyThrive
//
//  Created by Kirill Pyulzyu on 28.07.17.
//  Copyright © 2017 com.ololo. All rights reserved.
//

import UIKit

class TempDatasource {
    static let sharedInstance = TempDatasource()
    
    let firstName = "John"
    let lastName = "Bennet"
    let avatarImage = #imageLiteral(resourceName: "tempAvatar")
    let privateCredits = 5
    let enterpriseCredits = 2
    let complimentaryCredits = 10
    
    var initials:String {
        get{
            var str = ""
            if let firstName = self.firstName as String! {
                if firstName.characters.count > 0{
                    str.append(firstName[firstName.startIndex])
                }
            }

            if let lastName = self.lastName as String! {
                if lastName.characters.count > 0{
                    str.append(lastName[lastName.startIndex])
                }
            }

            return str.uppercased()
        }
    }
    
    var fullname:String{
        get{
            return (firstName + " " + lastName).trimmingCharacters(in: .whitespaces)
        }
    }
    
}
