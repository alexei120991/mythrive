//
// Created by Kirill Pyulzyu on 28.07.17.
// Copyright (c) 2017 com.ololo. All rights reserved.
//

import UIKit

class UIButtonsFactory {

    class func buttonWithRoundBorders(_ title: String) -> UIButton {
        let btn = UIButton(type: .custom)
        btn.layer.cornerRadius = 4
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.white.cgColor
        btn.setTitle(title)
        btn.titleLabel?.font = UIFont.centuryGothicRegular(17)
        btn.sizeToFit()
        btn.height = 54
        let btnHorizonatlPadding:CGFloat = 43
        btn.width = btn.width + btnHorizonatlPadding * 2.0
        return btn
    }
    
    
    class func buttonForMainMenu(_ title:String) -> UIButton{
        let btn = UIButton(type: .custom)
        btn.setTitleColor(UIColor(hex: "00a8a8"), for: .normal)
        btn.setTitleColor(UIColor.gray, for: .highlighted)
        btn.titleLabel!.font = UIFont.centuryGothicRegular(19.2)
        btn.setTitle(title.localized)
        btn.sizeToFit()
        return btn
    }
}
