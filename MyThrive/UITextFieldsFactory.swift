//
// Created by Kirill Pyulzyu on 28.07.17.
// Copyright (c) 2017 com.ololo. All rights reserved.
//

import UIKit

class UITextFieldsFactory {

    class func inputFieldWithPlaceholder(_ placeholder:String) -> UITextField{
        let tf = UITextField.init()

        let attrPlaceHolder = NSAttributedString(string: placeholder,
                attributes: [
                        NSFontAttributeName:UIFont.centuryGothicRegular(16),
                        NSForegroundColorAttributeName:UIColor.white
                ])

        tf.attributedPlaceholder = attrPlaceHolder

        tf.textColor = UIColor.white
        tf.width = 100
        tf.height = 46
        tf.backgroundColor = UIColor(hex: "33b9b9")
        tf.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]

        tf.leftView = UIView.init(frame: CGRect(x: 0, y: 0, width: 14, height: 10))
        tf.rightView = UIView.init(frame: CGRect(x: 0, y: 0, width: 14, height: 10))
        tf.leftViewMode = .always
        tf.rightViewMode = .always


        return tf
    }

}
