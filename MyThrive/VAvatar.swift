//
//  VAvatar.swift
//  MyThrive
//
//  Created by Kirill Pyulzyu on 01.08.17.
//  Copyright © 2017 com.ololo. All rights reserved.
//

import UIKit

class VAvatar:UIView{
    
    var lbl:UILabel = UILabel()
    var iv:UIImageView = UIImageView()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initUI(){
        
        self.layer.cornerRadius = self.width/2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 4
        self.clipsToBounds = true
        
        self.lbl.frame = self.bounds
        self.lbl.font = UIFont.centuryGothicRegular(23)
        self.lbl.textColor = UIColor.white
        self.lbl.textAlignment = .center
        self.lbl.backgroundColor = UIColor.clear
        self.addSubview(lbl)
        
        self.iv.frame = self.bounds
        self.iv.contentMode = .scaleAspectFill
        self.addSubview(iv)
    }
    
    
    func update(with image:UIImage?, initials:String?){
        self.iv.image = image
        self.lbl.text = initials ?? ""
    }
}
