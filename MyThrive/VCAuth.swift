//
//  VCAuth.swift
//  MyThrive
//
//  Created by Kirill Pyulzyu on 27.07.17.
//  Copyright © 2017 com.ololo. All rights reserved.
//

import UIKit

class VCAuth: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        
        self.initUI()
        
    }
    
    func initUI() {
        self.view.backgroundColor = UIColor(hex: "00a8a8")
        self.view.addSubview(self.vContainer)
    }
    
    
    func onBtnRememberMeTapped() {
        self.btnRememberMe.isSelected = !self.btnRememberMe.isSelected
    }
    
    func onBtnLoginTapped() {
        print("LOGIN")
    }
    
    func onBtnRegisterTapped() {
        print("REGISTER")
    }
    
    func onBtnResetTapped() {
        print("RESET")
        let vc = VCResetPass()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //MARK: lazy
    lazy var vContainer: UIView = {
        var v = UIView(frame: self.view.bounds)
        v.addSubview(self.vLogo)
        v.addSubview(self.vLabel)
        v.addSubview(self.tfEmail)
        v.addSubview(self.tfPassword)
        v.addSubview(self.btnResetPassword)
        v.addSubview(self.btnRememberMe)
        v.addSubview(self.lblRememberMe)
        v.addSubview(self.btnLogin)
        v.addSubview(self.btnRegister)
        
        v.height = self.btnRegister.bottom
        
        v.autoresizingMask = [.flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin]
        v.center = self.view.center
        
        return v
    }()
    
    
    lazy var vLogo: UIImageView = {
        var iv = UIImageView(image: #imageLiteral(resourceName:"authLogo"))
        iv.centerX = self.view.width / 2
        iv.top = 0
        iv.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        return iv
    }()
    
    
    lazy var vLabel: UILabel = {
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = CGFloat(3.0)
        
        
        let attrs = [
            NSFontAttributeName: UIFont.centuryGothicRegular(17),
            NSForegroundColorAttributeName: UIColor.white,
            NSParagraphStyleAttributeName: paragraphStyle
        ]
        
        let attrString = NSAttributedString(string: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna".localized,
                                            attributes: attrs)
        
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.attributedText = attrString
        lbl.width = self.view.width * 0.78
        lbl.numberOfLines = 0
        lbl.height = lbl.sizeThatFits(CGSize(width: lbl.width, height: CGFloat(MAXFLOAT))).height
        lbl.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        lbl.centerX = self.view.width / 2
        lbl.top = self.vLogo.bottom + 31;
        return lbl
    }()
    
    
    lazy var tfEmail: UITextField = {
        
        var tf = UITextFieldsFactory.inputFieldWithPlaceholder("Email address".localized)
        tf.width = self.view.width * 0.82
        tf.delegate = self;
        tf.centerX = self.view.width / 2
        tf.top = self.vLabel.bottom + 32
        return tf
    }()
    
    lazy var tfPassword: UITextField = {
        
        var tf = UITextFieldsFactory.inputFieldWithPlaceholder("Password".localized)
        tf.width = self.view.width * 0.82
        tf.delegate = self
        tf.centerX = self.view.width / 2
        tf.top = self.tfEmail.bottom + 8
        return tf
        
    }()
    
    
    lazy var btnResetPassword: UIButton = {
        let btn = UIButton(type: .custom)
        
        let attrString = NSAttributedString(string: "Forgot your password?".localized, attributes: [
            NSFontAttributeName: UIFont.centuryGothicRegular(14.0),
            NSUnderlineStyleAttributeName: 1,
            NSForegroundColorAttributeName: UIColor.white
            ])
        
        btn.setAttributedTitle(attrString, for: .normal)
        btn.titleLabel?.adjustsFontSizeToFitWidth = true
        btn.titleLabel?.minimumScaleFactor = 0.5
        btn.sizeToFit()
        btn.left = self.tfPassword.left
        btn.centerY = self.btnRememberMe.centerY
        if (btn.right > self.lblRememberMe.left) {
            btn.width = btn.width - (btn.right - self.lblRememberMe.left)
        }
        
        btn.addTarget(self, action: #selector(onBtnResetTapped))
        return btn
    }()
    
    
    lazy var lblRememberMe: UILabel = {
        
        let lbl = UILabel()
        lbl.textColor = UIColor.white
        lbl.font = UIFont.centuryGothicRegular(14.0)
        lbl.text = "Remember me".localized
        lbl.sizeToFit()
        lbl.centerY = self.btnRememberMe.centerY
        lbl.right = self.btnRememberMe.left - 7.6
        return lbl
    }()
    
    
    lazy var btnRememberMe: UIButton = {
        let btn = UIButton(type: .custom)
        btn.width = 30
        btn.height = 30
        btn.setImage(#imageLiteral(resourceName:"authCheck"), for: .selected)
        btn.addTarget(self, action: #selector(onBtnRememberMeTapped))
        btn.right = self.tfPassword.right
        btn.top = self.tfPassword.bottom + 15
        btn.isSelected = true
        btn.backgroundColor = UIColor(hex: "33b9b9")
        btn.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        return btn
    }()
    
    
    lazy var btnLogin: UIButton = {
        //var btn = UIButton(type: .custom)
        var btn = UIButtonsFactory.buttonWithRoundBorders("Login".localized)
        btn.top = self.btnResetPassword.bottom + 43
        btn.centerX = self.view.width / 2
        btn.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        
        btn.addTarget(self, action: #selector(onBtnLoginTapped))
        
        return btn
    }()
    
    
    
    lazy var btnRegister: UIButton = {
        
        let attrString1 = NSAttributedString(string: "Not a member? ".localized, attributes: [
            NSFontAttributeName: UIFont.centuryGothicRegular(14.0),
            NSForegroundColorAttributeName: UIColor.white
            ])
        
        
        let attrString2 = NSAttributedString(string: "Register here".localized, attributes: [
            NSFontAttributeName: UIFont.centuryGothicRegular(14.0),
            NSUnderlineStyleAttributeName: 1,
            NSForegroundColorAttributeName: UIColor.white
            ])
        
        
        let attrStringSum = NSMutableAttributedString()
        attrStringSum.append(attrString1)
        attrStringSum.append(attrString2)
        
        let btn = UIButton(type: .custom)
        btn.setAttributedTitle(attrStringSum, for: .normal)
        btn.sizeToFit()
        
        btn.addTarget(self, action: #selector(onBtnRegisterTapped))
        
        btn.top = self.btnLogin.bottom + 48.4
        btn.centerX = self.view.width / 2
        
        return btn
    }()
    
    
}
