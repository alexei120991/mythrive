//
//  VCMenu.swift
//  MyThrive
//
//  Created by Kirill Pyulzyu on 28.07.17.
//  Copyright © 2017 com.ololo. All rights reserved.
//

import UIKit

class VCMenu:UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    func initUI(){
        self.view.backgroundColor = UIColor.black
        
        self.view.addSubview(self.vAvatar)
        self.vAvatar.update(with: TempDatasource.sharedInstance.avatarImage, initials: TempDatasource.sharedInstance.initials)
        
        
        self.view.addSubview(self.btnClose)
        self.btnClose.addTarget(self, action: #selector(onBtnCloseTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.lblFullName)
        self.lblFullName.text = TempDatasource.sharedInstance.fullname
        self.lblFullName.sizeToFit()
        
        
        self.view.addSubview(self.btnEditProfile)
        self.btnEditProfile.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.btnMySessions)
        self.btnMySessions.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.btnBookings)
        self.btnBookings.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.btnAddCredits)
        self.btnAddCredits.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.btnHelp)
        self.btnHelp.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.btnLogOut)
        self.btnLogOut.addTarget(self, action: #selector(onMenuButtonTapped), for: .touchUpInside)
        
        
        self.view.addSubview(self.bottomPartBackView)
        
        self.view.addSubview(self.vPrivateCredits)
        self.view.addSubview(self.vEnterpriseCredits)
        self.view.addSubview(self.vComplimantaryCredits)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        layoutMiddlePart()
        layoutBottomPart()
    }
    
    
    
    fileprivate func layoutMiddlePart() {
        let arr = [btnMySessions, btnBookings, btnAddCredits, btnHelp, btnLogOut]
        let partHeight = self.bottomPartBorderY - btnMySessions.top - self.view.height * 0.05
        
        let vSpace = (partHeight - btnMySessions.height * CGFloat(arr.count))/CGFloat(arr.count-1)
        
        var lastY = btnMySessions.top
        for v in arr {
            v.top = lastY
            lastY += v.height + vSpace
        }
        
        
    }

    fileprivate func layoutBottomPart() {
        let bottomPartHeight = self.view.height - self.bottomPartBorderY
        let vSpace = (bottomPartHeight - self.vPrivateCredits.height*3.0)/4.0
        
        var lastY = self.bottomPartBorderY + vSpace
        let arr = [self.vPrivateCredits, self.vEnterpriseCredits, self.vComplimantaryCredits]
        let leftPadding:CGFloat = 39.2
        for v in arr {
            v.left = leftPadding
            v.top = lastY
            lastY += v.height + vSpace
        }
    }
    
    
    func onMenuButtonTapped(_ sender:UIButton){
        switch(sender){
            case self.btnEditProfile:
            print("btnEditProfile")
            break;
        case self.btnMySessions:
            print("btnMySessions")
            break;
        case self.btnBookings:
            print("btnBookings")
            break;
        case self.btnAddCredits:
            print("btnAddCredits")
            break;
        case self.btnHelp:
            print("btnHelp")
            break;
        case self.btnLogOut:
            print("btnLogOut")
            break;
        default: break;
        }
    }
    

    func onBtnCloseTapped(){
        print("onBtnCloseTapped")
    }
    

    
    
    
    //MARK: lazy
    lazy var btnClose:UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "menuCloseIcon"))
        btn.sizeToFit()
        btn.center = CGPoint(x: 31, y: self.view.height * 0.08)
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        return btn
    }()
    
    
    lazy var vAvatar:VAvatar = {
        let v = VAvatar(frame: CGRect(x: 0, y: 0, width: 64.5, height: 64.5))
        v.top = self.view.height * 0.125
        v.right = self.view.width - 39.6
        v.autoresizingMask = [.flexibleBottomMargin, .flexibleLeftMargin]
        return v
    }()
    
    
    lazy var lblFullName:UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.centuryGothicRegular(19)
        lbl.textColor = UIColor.white
        lbl.text = " "
        lbl.sizeToFit()
        lbl.left = 39.2
        lbl.top = self.view.height * 0.15
        lbl.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        return lbl
    }()
    
    
    lazy var btnEditProfile:UIButton = {
        let btn = UIButton(type: .custom)
        btn.setTitleColor(UIColor(hex: "00a8a8"), for: .normal)
        btn.setTitleColor(UIColor.gray, for: .highlighted)
        btn.titleLabel!.font = UIFont.centuryGothicRegular(15.3)
        btn.setTitle("Edit profile".localized)

        btn.sizeToFit()
        btn.left = self.lblFullName.left
        btn.top = self.lblFullName.bottom - 2
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        
        return btn
    }()
    
    lazy var btnMySessions:UIButton = {
        let btn = UIButtonsFactory.buttonForMainMenu("My sessions")
        btn.left = self.btnEditProfile.left
        btn.top = self.btnEditProfile.bottom - 5 + self.view.height * 0.04
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]

        return btn
    }()
    
    

    
    
    lazy var btnBookings:UIButton = {
        let btn = UIButtonsFactory.buttonForMainMenu("Bookings")
        btn.left = self.btnEditProfile.left
        btn.top = self.btnMySessions.bottom + self.view.height * 0.03
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        
        return btn
    }()
    
    lazy var btnAddCredits:UIButton = {
        let btn = UIButtonsFactory.buttonForMainMenu("Add credits")
        btn.left = self.btnEditProfile.left
        btn.top = self.btnBookings.bottom + self.view.height * 0.04
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        
        return btn
    }()
    
    lazy var btnHelp:UIButton = {
        let btn = UIButtonsFactory.buttonForMainMenu("Help")
        btn.left = self.btnEditProfile.left
        btn.top = self.btnAddCredits.bottom + self.view.height * 0.04
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        
        return btn
    }()
    
    
    lazy var btnLogOut:UIButton = {
        let btn = UIButtonsFactory.buttonForMainMenu("Log out")
        btn.left = self.btnEditProfile.left
        btn.top = self.btnHelp.bottom + self.view.height * 0.04
        btn.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        
        return btn
    }()
    
    
    lazy var vPrivateCredits:CreditsView = {
        let v = CreditsView(title: "Private credit balance".localized, credits: TempDatasource.sharedInstance.privateCredits)
        return v
    }()
    
    
    lazy var vEnterpriseCredits:CreditsView = {
        let v = CreditsView(title: "Enterprise credit balance".localized, credits: TempDatasource.sharedInstance.enterpriseCredits)
        return v
    }()
    
    
    lazy var vComplimantaryCredits:CreditsView = {
        let v = CreditsView(title: "Complimentary credit balance".localized, credits: TempDatasource.sharedInstance.complimentaryCredits)
        return v
    }()
    
    
    lazy var bottomPartBackView:UIView = {
        let v = UIView(frame: CGRect(x:0.0 , y: self.bottomPartBorderY, width: self.view.width, height: self.view.height - self.bottomPartBorderY))
        v.backgroundColor = UIColor(hex: "00a8a8")
        v.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleTopMargin]
        return v
    }()
    
    
    var bottomPartBorderY: CGFloat {
        get {
            return self.view.height*0.63
        }
    }
}


class CreditsView:UIView{
    
    var lblTitle:UILabel!
    var lblCredits:UILabel!
    
    
    init(title:String, credits:Int) {
        super.init(frame:CGRect(x: 0, y: 0, width: 10, height: 10))
        
        self.lblTitle = UILabel()
        self.lblTitle.font = UIFont.centuryGothicRegular(17)
        self.lblTitle.textColor = UIColor.white
        self.lblTitle.text = title;
        self.lblTitle.sizeToFit()
        self.addSubview(self.lblTitle)
        
        
        self.lblCredits = UILabel()
        self.lblCredits.font = UIFont.centuryGothicBold(17)
        self.lblCredits.textColor = UIColor.white
        self.lblCredits.text = "\(credits) CREDITS".localized
        self.lblCredits.sizeToFit()
        self.lblCredits.left = lblCredits.left
        self.lblCredits.top = lblCredits.bottom
        self.addSubview(self.lblCredits)
        
        
        self.width = max(self.lblTitle.width, self.lblCredits.width)
        self.height = lblCredits.bottom
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




