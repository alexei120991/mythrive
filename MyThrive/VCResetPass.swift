//
// Created by Kirill Pyulzyu on 28.07.17.
// Copyright (c) 2017 com.ololo. All rights reserved.
//

import UIKit

class VCResetPass:UIViewController, UITextFieldDelegate{

    override func viewDidLoad(){
       
        self.initUI()
    }

    func initUI(){
        self.view.backgroundColor = UIColor(hex: "00a8a8")
        self.view.addSubview(self.vContainer)
    }

    func onBtnResetTapped(){
        print("RESET")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: lazy
    lazy var vContainer:UIView = {
        var v = UIView(frame: self.view.bounds)
        v.addSubview(self.vLogo)
        v.addSubview(self.vLabelReset)
        v.addSubview(self.vLabel)
        v.addSubview(self.tfEmail)
        v.addSubview(self.btnReset)
        
        v.height = self.btnReset.bottom
        
        v.autoresizingMask = [.flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin]
        v.top = self.view.centerY - self.tfEmail.centerY
        
        return v
    }()
    
    
    lazy var vLogo:UIImageView = {
        var iv = UIImageView(image: #imageLiteral(resourceName: "authLogo"))
        iv.centerX = self.view.width/2
        iv.top = 0
        iv.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        return iv
    }()
    
    
    lazy var vLabelReset:UILabel = {
        
        let attrs = [
            NSFontAttributeName:UIFont.centuryGothicRegular(23),
            NSForegroundColorAttributeName:UIColor.white,
            ]
        
        let attrString = NSAttributedString(string: "Reset your password".localized,
                                            attributes: attrs)
        
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.attributedText = attrString
        lbl.width = self.view.width*0.78
        lbl.numberOfLines = 0
        lbl.height = lbl.sizeThatFits(CGSize(width: lbl.width, height: CGFloat(MAXFLOAT))).height
        lbl.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        lbl.centerX = self.view.width/2
        lbl.top = self.vLogo.bottom+30;
        return lbl
    }()
    
    lazy var vLabel:UILabel = {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = CGFloat(3.0)
        
        
        let attrs = [
            NSFontAttributeName:UIFont.centuryGothicRegular(17),
            NSForegroundColorAttributeName:UIColor.white,
            NSParagraphStyleAttributeName:paragraphStyle
        ]
        
        let attrString = NSAttributedString(string: "Simply enter the email adress you registered with, and we’ll send you a link to reset it.".localized,
                                            attributes: attrs)
        
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.attributedText = attrString
        lbl.width = self.view.width*0.78
        lbl.numberOfLines = 0
        lbl.height = lbl.sizeThatFits(CGSize(width: lbl.width, height: CGFloat(MAXFLOAT))).height
        lbl.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        lbl.centerX = self.view.width/2
        lbl.top = self.vLabelReset.bottom+10;
        return lbl
    }()
    
    
    lazy var tfEmail:UITextField = {
        var tf = UITextFieldsFactory.inputFieldWithPlaceholder("Email address".localized)
        tf.width = self.view.width*0.82
        tf.centerX = self.view.width/2
        tf.top = self.vLabel.bottom + 29.6
        tf.delegate = self
        return tf
    }()
    
    
    lazy var btnReset:UIButton = {
        var btn = UIButtonsFactory.buttonWithRoundBorders("Reset my password".localized)
        btn.centerX = self.view.width/2
        btn.top = self.tfEmail.bottom + 34.2
        btn.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        
        btn.addTarget(self, action: #selector(onBtnResetTapped))
        
        return btn
    }()
    
}


